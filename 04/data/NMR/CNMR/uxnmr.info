CONFIGURATION INFORMATION
=========================

Path         : /opt/topspin3.2/conf/instr/spect/uxnmr.info
Date         : Fri Jul 17 10:32:36 2020
Release      : TOPSPIN Version 3.2-pl3
Installed in : /opt/topspin3.2
Host         : nmrs5
OS           : CentOS release 5.11 (Final)
CPU          : Intel(R) Xeon(R) CPU E5-1620 0 @ 3.60GHz (8 cores at 1200 MHz with Hyperthreading)
User         : autosampler (Autosampler)
Description  : AVANCE NANOBAY V3-E 200MHz
Location     : ETHZ 118.D
System       : Avance III NMR spectrometer
1H-frequency : 200.13 MHz
Order Number : 10095951
Configured in: /opt/topspin3.2/conf/instr/spect

IPSO: connected to spectrometer subnet
- TCP/IP address = 149.236.99.254
- Tctrl : 1
- Fctrls: 2
- Gctrl1: without digital preemphasis
- Rctrl : 1
- FREDs : none
- DPP : none

DRU: AQS DRU Z100977/02171 ECL 07.01
- TCP/IP address = 149.236.99.89
- Firmware Version = 151014
- DRU controls AQS-Rack and HPPR/2

AQS: connected to 149.236.99.89:/dev/tty10
  _Slot_ SBSB _____________________Board_____________________
  Number Addr Type HW-VS FW-VS ID  ECL Name   Description
  -----------------------------------------------------------
     2   0x10 0x43   0x5    CD  R  0.2 REC-1  AQS RXAD/2 600 Z130588/465 ECL 00.02
     3   0x34 0xd6   0x1        X  2.0 REF-1  REF/3-600 Reference Board for AQS/3 Receiver
     4   0x24 0x32     0        S  2.1 SGU-1  AQS SGU/3 600 (2CH) Z117129/00579 ECL 02.01 FPGA-Vs=20121030
     8   0x36  0x5     0        A  9.2 BLA-1  BLA2BB 150/60 20-400 W1345049/3195 ECL 09.02
    12   0xa0 0x94     0        V  2.0 PS-1   
    14   0xa1 0x93     0        V  0.1 PS-2   
    15   0xa2 0x91     0        V  1.2 PS-3   AQS PSM-D Power Supply Module
    --   0x20 0xd9     0        B  1.0 MASTER 
     1   0x20  0x7     0        B      MASTER AQS Rack Master
  Logical Receiver/SGU configuration:


Router: 1 AQS-Minirouter

Amplifiers (AQS configuration):
  RO  Amplifier  Module  Nucleus  Power/W  Switchbox Name PN/SN/FW
   1      1         1        B      150    N         BLA2BB 150/60 20-400 W1345049/3195/-
   2      1         2        B       60    N         BLA2BB 150/60 20-400 W1345049/3195/-


Transmitters at the spectrometer subnet:
----------------------------------------
LTRX Z109886/00010 ECL 01.01:
- TCP/IP address = 149.236.99.20
- Amplifier      = BSMS/2 LOCK TRANSCEIVER 200: Z109886/00010 ECL 01.01

BSMS: BSMS/2 connected to ethernet
- TCP/IP address = 149.236.99.20
- ELCB firmware version = 20130320
- GAB current limits = 0.0/X, 0.0/Y, 10.0/Z (in A)
- SCB channels = 20
- Shim System = BOSS1-SB
- L-TRX = BSMS/2 LOCK TRANSCEIVER 200: Z109886/00010 ECL 01.01
- Lock: on L-TRX board
- VTU_SPB = BSMS/2 SPB SENSOR & PNEUMATIC BD: Z115191/01327 ECL 04.00
- VTU_VPSB1 = BSMS/2 VARIABLE POWER SUPPLY BD: Z115193/00065 ECL 01.02

VTU: in BSMS/2 connected to ethernet
- TCP/IP address = 149.236.99.20

Bruker Automatic Changer: device connected to 149.236.99.254:/dev/tty01
- Capacity: 60 holders
- Firmware: 20120906
- use BACS air  = yes
- BACS sx delay = 5 s
- Sample Rail fast changer mode = no

Sample Changers at the spectrometer subnet:
-------------------------------------------
 - BACS2_H15000-01_0296: at TCP/IP address 149.236.99.148


Preamplifiers :
HPPR: - HPPR/2 preamplifier connected to 149.236.99.89:/dev/tty10
    Type      : HPPR/2
    Controller: Application firmware = c.
                no LED display for tuning and matching
    Module  1 : 1H
                 PN=Z003456, SN=00018 from 20130327
    Module  2 : XBB19F 2HS
                 PN=Z003500, SN=00017 from 20130327
    Module  3 : 2H
                 PN=Z003467, SN=00019 from 20130307


Frequency generation:
- F1: for SGU
- F2: for SGU



RF cable connections (detected by 'confamp')
--------------------------------------------------------------------
SGU1 NORM output -> input 1 of transmitter 1 (BLA2BB 150/60 20-400 W1345049/3195)
SGU1 AUX  output -> input 1 of transmitter 2 (BSMS/2 LOCK TRANSCEIVER 200 Z109886/00010 at TCP/IP 149.236.99.20)
SGU2 NORM output -> input 2 of transmitter 1 (BLA2BB 150/60 20-400 W1345049/3195)
SGU2 AUX  output -> TUNE signal input of HPPR

Blanking cable connections (detected by 'confamp')
--------------------------------------------------------------------
transmitter 1 (BLA2BB 150/60 20-400 W1345049/3195) amplifier B-150W uses blanking 1
transmitter 1 (BLA2BB 150/60 20-400 W1345049/3195) amplifier B-60W uses blanking 2
transmitter 2 (BSMS/2 LOCK TRANSCEIVER 200 Z109886/00010 at TCP/IP 149.236.99.20) amplifier 2H-5W uses blanking 8


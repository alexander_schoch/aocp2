##TITLE= Audit trail, TOPSPIN		Version 3.2
##JCAMPDX= 5.01
##ORIGIN= Bruker BioSpin GmbH
##OWNER= autosampler
$$ /opt/topspin3.2/data/oacpract2/nmr/H194-156-V04/3/audita.txt
##AUDIT TRAIL=  $$ (NUMBER, WHEN, WHO, WHERE, PROCESS, VERSION, WHAT)
(   1,<2020-11-21 04:43:43.358 +0100>,<autosampler>,<nmrs5>,<go>,<TOPSPIN 3.2>,
      <created by zg
	started at 2020-11-21 03:21:38.053 +0100,
	POWCHK enabled, PULCHK disabled,
       configuration hash MD5:
       E1 C4 EE C3 B3 61 FC 8E 92 5C 29 FA AA F4 26 3D
       data hash MD5: 64K
       83 71 44 B8 84 24 D9 9D 70 34 E9 A2 C0 C9 B1 9B>)
(   2,<2020-11-21 04:43:43.620 +0100>,<autosampler>,<nmrs5>,<audit>,<TOPSPIN 3.2>,
      <user comment:
       ICON-NMR User ID: oacpract2
       data hash MD5: 64K
       83 71 44 B8 84 24 D9 9D 70 34 E9 A2 C0 C9 B1 9B>)
(   3,<2020-11-21 04:43:43.630 +0100>,<autosampler>,<nmrs5>,<audit>,<TOPSPIN 3.2>,
      <user comment:
       ICON-NMR User ID: oacpract2
       data hash MD5: 64K
       83 71 44 B8 84 24 D9 9D 70 34 E9 A2 C0 C9 B1 9B>)
##END=

$$ hash MD5
$$ C1 5A F3 57 8F CE 41 A5 7F 9B 3E 59 BA F9 9F A3

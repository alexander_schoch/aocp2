##TITLE= Audit trail, TOPSPIN		Version 3.2
##JCAMPDX= 5.01
##ORIGIN= Bruker BioSpin GmbH
##OWNER= autosampler
$$ /opt/topspin3.2/data/oacpract2/nmr/H194-156-V05/6/audita.txt
##AUDIT TRAIL=  $$ (NUMBER, WHEN, WHO, WHERE, PROCESS, VERSION, WHAT)
(   1,<2020-11-28 04:11:53.568 +0100>,<autosampler>,<nmrs5>,<go>,<TOPSPIN 3.2>,
      <created by zg
	started at 2020-11-28 03:45:23.686 +0100,
	POWCHK enabled, PULCHK disabled,
       configuration hash MD5:
       E1 C4 EE C3 B3 61 FC 8E 92 5C 29 FA AA F4 26 3D
       data hash MD5: 64K
       12 61 90 54 35 70 84 58 AA C8 55 CB D0 58 09 7D>)
(   2,<2020-11-28 04:11:53.831 +0100>,<autosampler>,<nmrs5>,<audit>,<TOPSPIN 3.2>,
      <user comment:
       ICON-NMR User ID: oacpract2
       data hash MD5: 64K
       12 61 90 54 35 70 84 58 AA C8 55 CB D0 58 09 7D>)
(   3,<2020-11-28 04:11:53.840 +0100>,<autosampler>,<nmrs5>,<audit>,<TOPSPIN 3.2>,
      <user comment:
       ICON-NMR User ID: oacpract2
       data hash MD5: 64K
       12 61 90 54 35 70 84 58 AA C8 55 CB D0 58 09 7D>)
##END=

$$ hash MD5
$$ A4 5A CF B4 17 24 15 AF 7A FA 94 C1 0B D2 62 1D

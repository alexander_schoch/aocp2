##TITLE= Audit trail, TOPSPIN		Version 3.2
##JCAMPDX= 5.01
##ORIGIN= Bruker BioSpin GmbH
##OWNER= autosampler
$$ /opt/topspin3.2/data/oacpract2/nmr/H194-156-V05/1/pdata/1/auditp.txt
##AUDIT TRAIL=  $$ (NUMBER, WHEN, WHO, WHERE, PROCESS, VERSION, WHAT)
(   1,<2020-11-23 16:38:04.943 +0100>,<autosampler>,<nmrs5>,<go>,<TOPSPIN 3.2>,
      <created by zg
	started at 2020-11-23 16:35:17.583 +0100,
	POWCHK enabled, PULCHK disabled,
       configuration hash MD5:
       E1 C4 EE C3 B3 61 FC 8E 92 5C 29 FA AA F4 26 3D
       data hash MD5: 64K
       7E 2B C2 57 46 57 97 DD A8 AB FB 21 88 1D 39 F1>)
(   2,<2020-11-23 16:38:05.285 +0100>,<autosampler>,<nmrs5>,<audit>,<TOPSPIN 3.2>,
      <user comment:
       ICON-NMR User ID: oacpract2
       data hash MD5: 64K
       7E 2B C2 57 46 57 97 DD A8 AB FB 21 88 1D 39 F1>)
(   3,<2020-11-23 16:38:05.295 +0100>,<autosampler>,<nmrs5>,<audit>,<TOPSPIN 3.2>,
      <user comment:
       ICON-NMR User ID: oacpract2
       data hash MD5: 64K
       7E 2B C2 57 46 57 97 DD A8 AB FB 21 88 1D 39 F1>)
(   4,<2020-11-23 16:38:05.780 +0100>,<autosampler>,<nmrs5>,<proc1d>,<TOPSPIN 3.2>,
      <Start of raw data processing
       ef LB = 0.3 FT_mod = 6 PKNL = 1 SI = 64K 
       data hash MD5: 64K
       DE A1 2F EE 4A E5 0D 70 E3 E5 C1 A1 D2 31 78 D7>)
(   5,<2020-11-23 16:38:05.957 +0100>,<autosampler>,<nmrs5>,<proc1d>,<TOPSPIN 3.2>,
      <apk 
       data hash MD5: 64K
       70 BF 53 2D 34 21 F6 9F A2 21 26 B4 EB 85 52 67>)
(   6,<2020-11-23 16:38:06.015 +0100>,<autosampler>,<nmrs5>,<proc1d>,<TOPSPIN 3.2>,
      <abs ABSG = 5 
       data hash MD5: 64K
       72 4F 68 F5 47 6B DE 43 02 28 91 CA 66 E7 28 67>)
##END=

$$ hash MD5
$$ C4 A4 8F 3A 38 E3 71 3F 61 E8 B5 80 86 33 9F 4A

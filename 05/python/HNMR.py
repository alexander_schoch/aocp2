# Import libraries: numpy for advanced math stuff, matplotlib.pyplot for creating fancy plots, nmrglue for nmr stuff
import numpy as np
import matplotlib.pyplot as plt
import nmrglue as ng

# Latex implementation: Process all text with latex
plt.rc('text', usetex=True)
plt.rc('text.latex', preamble=r'\usepackage{siunitx}\usepackage{sfmath}')

# read in the bruker formatted data
dic, data = ng.bruker.read('../data/NMR/HNMR')

# generate a universal directory with the parameters for this specific spectrometer
udic = ng.bruker.guess_udic(dic, data)

# generate unit conversion
uc = ng.fileio.fileiobase.unit_conversion(udic[0]['size'], udic[0]['complex'], udic[0]['sw'], udic[0]['obs'], udic[0]['car'])

# remove the digital filter (this is afaik a bug in bruker data)
data = ng.bruker.remove_digital_filter(dic, data)

# process the spectrum
data = ng.proc_base.zf_size(data, 32768)      # zero fill to 32768 points (speeds up fft)
data = ng.proc_base.fft(data)                 # fourier transform
data = ng.proc_autophase.autops(data, 'acme') # phase correction (automatic)
data = ng.proc_base.rev(data)                 # reverse plot
data = data / max(data)                       # normalize intensity to 1

# get ppm scale
ppm = uc.ppm_scale()

# allows more fiddling with the plot
fig, ax = plt.subplots()

# get peaks
peaks = ng.analysis.peakpick.pick(np.real(data), 0.03, msep=(300), algorithm='thres')
peak_list = []
for tup in peaks:
    peak_list.append([str(round(uc.ppm(tup[0]), 3)), uc.ppm(tup[0]) + 0.15, uc.ppm(tup[0]) - 0.15])

# draw integrals and shifts
integrals = []
for name, start, end in peak_list:
    minimum = uc(start, "ppm")
    maximum = uc(end, "ppm")

    if minimum > maximum:
        minimum, maximum = maximum, minimum

    mid = uc.ppm(minimum + (maximum - minimum) / 2)
    # extract the peak
    peak = data[minimum:maximum + 1]
    peak_scale = uc.ppm_scale()[minimum:maximum + 1]

    # plot the integration lines, limits and name of peaks
    ax.plot(peak_scale, np.real(peak.cumsum()) / 100. + np.real(peak.max()), color='black', linewidth=.1)
    integrals.append([np.real(peak.cumsum())[-1], mid])
    ax.text(mid, 0.5 * np.real(peak.sum()) / 100. + np.real(peak.max()) + 0.05, name, fontsize=8, ha='center', rotation='vertical')


integrals = np.array(integrals)
for row in integrals:
    value = row[0] / max(integrals[:,0]) * 21
    ax.text(row[1], -.04, str(round(value, 2)), ha='center', fontsize=7)


# solvent
#lsm = peaks[2]
#ax.text(uc.ppm(lsm[0]), np.real(peak.sum() / 100. + np.real(peak.max()) + 0.5), r'CHCl\textsubscript{3}', ha='center', rotation='vertical')
#

# remove y axis
ax.get_yaxis().set_visible(False)

# remove spines ("frame")
ax.spines['top'].set_visible(False)
ax.spines['right'].set_visible(False)
ax.spines['left'].set_visible(False)

# invert x axis
ax.invert_xaxis()

# plot data
ax.plot(ppm, np.real(data), linewidth=.5, color='black')

# zoom in on a region
plt.xlim(8.5,-.5)
#plt.ylim(-.01,.3)

# Axis label
plt.xlabel(r'$\delta_\text{H}$ / ppm', fontsize=16)

# make margins nice and print the plot to a pdf file
fig.tight_layout()
fig.savefig('../plots/HNMR.pdf')

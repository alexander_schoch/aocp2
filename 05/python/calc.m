M = [210.27 208.25 54.024];
equiv = [1 1 2.22];

starting = 10; # g of [0]

#m = one * equiv .* M

measured = [10.057 9.9055 5.7028];
one = measured(1) / M(1); # mol
n = measured ./ M;
equiv = n / one;

########

rhoBr2 = 3.1
M1 = [400.51 159.81];
equiv = [1 2.6];
n1 = n(1);
m1 = M1 .* equiv * n1
m1(2) / rhoBr2

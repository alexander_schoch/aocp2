# Import libraries: numpy for advanced math stuff, matplotlib.pyplot for creating fancy plots, nmrglue for nmr stuff
import numpy as np
import matplotlib.pyplot as plt
import nmrglue as ng

# Latex implementation: Process all text with latex
plt.rc('text', usetex=True)
plt.rc('text.latex', preamble=r'\usepackage{siunitx}\usepackage[version=4]{mhchem}')

# read in the bruker formatted data
fig, ax = plt.subplots()
ax1 = plt.subplot(211)
ax2 = plt.subplot(212)

def cnmr():
    dic, data = ng.bruker.read('../data/NMR/CNMR')

    # generate a universal directory with the parameters for this specific spectrometer
    udic = ng.bruker.guess_udic(dic, data)

    # generate unit conversion
    uc = ng.fileio.fileiobase.unit_conversion(udic[0]['size'], udic[0]['complex'], udic[0]['sw'], udic[0]['obs'], udic[0]['car'])

    # remove the digital filter
    data = ng.bruker.remove_digital_filter(dic, data)

    # process the spectrum
    data = ng.proc_base.zf_size(data, 32768)      # zero fill to 32768 points (speeds up fft)
    data = ng.proc_base.fft(data)                 # fourier transform
    data = ng.proc_autophase.autops(data, 'acme') # phase correction (automatic)
    data = ng.proc_base.rev(data)
    data = data / max(data)                       # normalize intensity to 1

    # get ppm scale
    ppm = uc.ppm_scale()

    # allows more fiddling with the plot

    # get peaks
    peaks = ng.analysis.peakpick.pick(np.real(data), 0.1, msep=(200), algorithm='thres')
    peak_list = []
    for tup in peaks:
        peak_list.append([str(round(uc.ppm(tup[0]), 3)), uc.ppm(tup[0]) + 0.1, uc.ppm(tup[0]) - 0.1])

    # draw shifts
    for name, start, end in peak_list:
        minimum = uc(start, "ppm")
        maximum = uc(end, "ppm")

        if minimum > maximum:
            minimum, maximum = maximum, minimum

        mid = uc.ppm(minimum + (maximum - minimum) / 2)
        # extract the peak
        peak = data[minimum:maximum + 1]
        peak_scale = uc.ppm_scale()[minimum:maximum + 1]

        # plot the integration lines, limits and name of peaks
        ax1.text(mid, 0.5 * np.real(peak.sum()) / 100. + np.real(peak.max()) + 0.05, name, fontsize=8, ha='center', rotation='vertical')

    # solvent
    lsm = peaks[0]
    ax1.text(uc.ppm(lsm[0]), 0.5 * np.real(peak.sum() / 100. + np.real(peak.max()) + .7), r'CHCl\textsubscript{3}', ha='center', rotation='vertical')
    ax1.plot(ppm, np.real(data), linewidth=.5, color='black')

def dept():
    dic, data = ng.bruker.read('../data/NMR/DEPT135')

    # generate a universal directory with the parameters for this specific spectrometer
    udic = ng.bruker.guess_udic(dic, data)

    # generate unit conversion
    uc = ng.fileio.fileiobase.unit_conversion(udic[0]['size'], udic[0]['complex'], udic[0]['sw'], udic[0]['obs'], udic[0]['car'])

    # remove the digital filter
    data = ng.bruker.remove_digital_filter(dic, data)

    # process the spectrum
    data = ng.proc_base.zf_size(data, 32768)      # zero fill to 32768 points (speeds up fft)
    data = ng.proc_base.fft(data)                 # fourier transform
    data = ng.proc_autophase.autops(data, 'acme') # phase correction (automatic)
    data = ng.proc_base.rev(data)
    data = data / max(data)                       # normalize intensity to 1

    # get ppm scale
    ppm = uc.ppm_scale()

    # get peaks
    peaks = ng.analysis.peakpick.pick(np.real(data), 0.1, msep=(200), algorithm='thres')
    peak_list = []
    for tup in peaks:
        peak_list.append([str(round(uc.ppm(tup[0]), 3)), uc.ppm(tup[0]) + 0.1, uc.ppm(tup[0]) - 0.1])

    # draw shifts
    for name, start, end in peak_list:
        minimum = uc(start, "ppm")
        maximum = uc(end, "ppm")

        if minimum > maximum:
            min, maximum = max, min

        mid = uc.ppm(minimum + (maximum - minimum) / 2)
        # extract the peak
        peak = data[minimum:maximum + 1]
        peak_scale = uc.ppm_scale()[minimum:maximum + 1]

        # plot the integration lines, limits and name of peaks
        ax2.text(mid, 0.5 * np.real(peak.sum()) / 100. + np.real(peak.max()) + 0.05, name, fontsize=8, ha='center', rotation='vertical')

    # how to interpret
    ax2.text(0,.5,r'DEPT-135:\par\medskip \ce{CH3} / \ce{CH} $\uparrow$\par\smallskip \ce{CH2} $\downarrow$',ha='right', va='center', fontsize=16)
    ax2.plot(ppm, np.real(data), linewidth=.5, color='black')

cnmr()
dept()


# remove y axis
ax1.get_yaxis().set_visible(False)
ax2.get_yaxis().set_visible(False)

# remove spines ("frame")
ax1.spines['top'].set_visible(False)
ax1.spines['right'].set_visible(False)
ax1.spines['left'].set_visible(False)
ax2.spines['top'].set_visible(False)
ax2.spines['right'].set_visible(False)
ax2.spines['left'].set_visible(False)

# invert x axis
ax1.invert_xaxis()
ax2.invert_xaxis()

# zoom in on a region
ax1.set_xlim(140,0)
ax2.set_xlim(140,0)

# Axis label
plt.xlabel(r'$\delta_\text{C}$ / ppm', fontsize=16)

# make margins nice and print the plot to a pdf file
fig.tight_layout()
fig.savefig('../plots/CNMR_comb.pdf')

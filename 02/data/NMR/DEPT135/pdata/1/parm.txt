
Current Data Parameters
NAME       H194-156-V02              
EXPNO                 8              
PROCNO                1              

F2 - Acquisition Parameters
Date_          20201109              
Time              23.43              
INSTRUM           spect              
PROBHD   5 mm PABBO BB-              
PULPROG       deptsp135              
TD                65536              
SOLVENT           CDCl3              
NS                  256              
DS                    4              
SWH            8064.516 Hz           
FIDRES         0.123055 Hz           
AQ            4.0632319 sec          
RG               200.48              
DW               62.000 usec         
DE                10.00 usec         
TE                298.1 K            
CNST2       145.0000000              
D1           2.00000000 sec          
D2           0.00344828 sec          
D12          0.00002000 sec          
TD0                   1              

======== CHANNEL f1 ========
SFO1         50.3267541 MHz          
NUC1                13C              
P1                 9.60 usec         
P13             2000.00 usec         
PLW0     0 W            
PLW1        30.00000000 W            
SPNAM[5]    Crp60comp.4              
SPOAL5            0.500              
SPOFFS5  0 Hz           
SPW5         4.22429991 W            

======== CHANNEL f2 ========
SFO2        200.1306400 MHz          
NUC2                 1H              
CPDPRG[2        waltz16              
P3                12.50 usec         
P4                25.00 usec         
PCPD2             90.00 usec         
PLW2        12.00000000 W            
PLW12        0.23148000 W            

F2 - Processing parameters
SI                32768              
SF           50.3227290 MHz          
WDW                  EM              
SSB      0              
LB                 1.00 Hz           
GB       0              
PC                 1.40              

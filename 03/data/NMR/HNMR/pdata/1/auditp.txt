##TITLE= Audit trail, TOPSPIN		Version 3.2
##JCAMPDX= 5.01
##ORIGIN= Bruker BioSpin GmbH
##OWNER= autosampler
$$ /opt/topspin3.2/data/oacpract2/nmr/H194-156-V03-2/1/pdata/1/auditp.txt
##AUDIT TRAIL=  $$ (NUMBER, WHEN, WHO, WHERE, PROCESS, VERSION, WHAT)
(   1,<2020-11-17 14:59:40.630 +0100>,<autosampler>,<nmrs5>,<go>,<TOPSPIN 3.2>,
      <created by zg
	started at 2020-11-17 14:56:53.270 +0100,
	POWCHK enabled, PULCHK disabled,
       configuration hash MD5:
       E1 C4 EE C3 B3 61 FC 8E 92 5C 29 FA AA F4 26 3D
       data hash MD5: 64K
       9D 03 D8 D6 5B 8C E5 6C 51 6D 75 FD E0 CE B7 FC>)
(   2,<2020-11-17 14:59:40.961 +0100>,<autosampler>,<nmrs5>,<audit>,<TOPSPIN 3.2>,
      <user comment:
       ICON-NMR User ID: oacpract2
       data hash MD5: 64K
       9D 03 D8 D6 5B 8C E5 6C 51 6D 75 FD E0 CE B7 FC>)
(   3,<2020-11-17 14:59:40.971 +0100>,<autosampler>,<nmrs5>,<audit>,<TOPSPIN 3.2>,
      <user comment:
       ICON-NMR User ID: oacpract2
       data hash MD5: 64K
       9D 03 D8 D6 5B 8C E5 6C 51 6D 75 FD E0 CE B7 FC>)
(   4,<2020-11-17 14:59:41.430 +0100>,<autosampler>,<nmrs5>,<proc1d>,<TOPSPIN 3.2>,
      <Start of raw data processing
       ef LB = 0.3 FT_mod = 6 PKNL = 1 SI = 64K 
       data hash MD5: 64K
       46 15 25 8F 02 86 C2 51 6D 74 D0 58 66 97 55 7C>)
(   5,<2020-11-17 14:59:41.670 +0100>,<autosampler>,<nmrs5>,<proc1d>,<TOPSPIN 3.2>,
      <apk 
       data hash MD5: 64K
       C1 42 E9 41 B7 76 56 74 98 1F DD 4E 5F 84 24 AA>)
(   6,<2020-11-17 14:59:41.730 +0100>,<autosampler>,<nmrs5>,<proc1d>,<TOPSPIN 3.2>,
      <abs ABSG = 5 
       data hash MD5: 64K
       1B 3F F4 A7 D3 03 43 DD BF 01 0D DB 9A 93 19 F4>)
##END=

$$ hash MD5
$$ 2D B2 6C 19 09 0B 93 76 48 FD 4D C8 D8 B7 98 72

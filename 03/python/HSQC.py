# Import libraries: numpy for advanced math stuff, matplotlib.pyplot for creating fancy plots, nmrglue for nmr stuff
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.cm
import nmrglue as ng
import pandas as pd

# Latex implementation: Process all text with latex
plt.rc('text', usetex=True)
plt.rc('text.latex', preamble=r'\usepackage{siunitx}\usepackage{sfmath}')

# plot parameters
cmap = matplotlib.cm.Greys      # contour map (colors to use for contours)
contour_start = 0.08            # contour level start value
contour_num = 20                # number of contour levels
contour_factor = 1.01           # scaling factor between contour levels

# limits
hlimit = [8, 1]
climit = [150, 0]
hnmrlimit = [-.05, .5]
cnmrlimit = [-.05, .5]

# calculate contour levels, aka. at z = what should i draw a line?
cl = contour_start * contour_factor ** np.arange(contour_num) 

# read in the bruker formatted data
dic, dataraw = ng.bruker.read('../data/NMR/HSQC/')

# calculate universal dictionary (e.g. spectrometer freq)
udic = ng.bruker.guess_udic(dic, dataraw)
uc0 = ng.fileiobase.uc_from_udic(udic, 0)
uc1 = ng.fileiobase.uc_from_udic(udic, 1)

# convert all data to NMRPipe, 'cause bruker data is just shit to work with
C = ng.convert.converter()
C.from_bruker(dic, dataraw)
dic, data = C.to_pipe()

# process the direct dimension
dic, data = ng.pipe_proc.sp(dic, data, off=0.35, end=0.98, pow=1, c=1.0)
dic, data = ng.pipe_proc.zf(dic, data, auto=True)
dic, data = ng.pipe_proc.ft(dic, data, auto=True)
dic, data = ng.pipe_proc.ps(dic, data, p0=-29.0, p1=0.0)
dic, data = ng.pipe_proc.di(dic, data)
dic, data = ng.pipe_proc.rev(dic, data)

# process the indirect dimension
dic, data = ng.pipe_proc.tp(dic, data)
dic, data = ng.pipe_proc.sp(dic, data, off=0.35, end=0.9, pow=1, c=0.5)
dic, data = ng.pipe_proc.zf(dic, data, size=2048)
dic, data = ng.pipe_proc.ft(dic, data, auto=True)
dic, data = ng.pipe_proc.ps(dic, data, p0=0.0, p1=0.0)
dic, data = ng.pipe_proc.di(dic, data)
dic, data = ng.pipe_proc.tp(dic, data)

# normalize data to be 0-1
data = data / max([max(l) for l in data])

# calculate ppm scale
ppm0 = uc0.ppm_scale()
ppm0s = uc0.ppm_limits()
ppm1 = uc1.ppm_scale()
ppm1s = uc0.ppm_limits()

# create the 3 plots
axtop = plt.subplot2grid((5, 5), (0, 0), colspan=4)
axmain = plt.subplot2grid((5, 5), (1, 0), colspan=4, rowspan=4)
axright = plt.subplot2grid((5, 5), (1, 4), rowspan=4)

# plot the contours - COSY
axmain.contour(np.real(data), cl, cmap=cmap, extent=(ppm0s[1], ppm0s[0], ppm1s[1], ppm1s[0]), linewidths=.1)

# plot the HNMRs, the function allows me to use the variable names again without overwriting (local variables)
def hnmr():
    dic, data = ng.bruker.read('../data/NMR/HNMR')

    # generate a universal directory with the parameters for this specific spectrometer
    udic = ng.bruker.guess_udic(dic, data)

    # generate unit conversion
    uc = ng.fileio.fileiobase.unit_conversion(udic[0]['size'], udic[0]['complex'], udic[0]['sw'], udic[0]['obs'], udic[0]['car'])

    # remove the digital filter (this is afaik a bug in bruker data)
    data = ng.bruker.remove_digital_filter(dic, data)

    # process the spectrum
    data = ng.proc_base.zf_size(data, 32768)      # zero fill to 32768 points (speeds up fft)
    data = ng.proc_base.fft(data)                 # fourier transform
    data = ng.proc_autophase.autops(data, 'acme') # phase correction (automatic)
    data = ng.proc_base.rev(data)                 # reverse plot
    data = data / max(data)                       # normalize intensity to 1

    # get ppm scale
    ppm = uc.ppm_scale()

    axtop.set_xlim(hlimit[0], hlimit[1])
    #axright.set_ylim(climit[0], climit[1])
    #axright.set_xlim(cnmrlimit[0], cnmrlimit[1])
    axtop.set_ylim(hnmrlimit[0], hnmrlimit[1])
    axtop.plot(ppm, np.real(data), linewidth=.5, color='black')
    #axright.plot(np.real(data), ppm, linewidth=.5, color='black')

def cnmr():
    dic, data = ng.bruker.read('../data/NMR/CNMR')

    # generate a universal directory with the parameters for this specific spectrometer
    udic = ng.bruker.guess_udic(dic, data)

    # generate unit conversion
    uc = ng.fileio.fileiobase.unit_conversion(udic[0]['size'], udic[0]['complex'], udic[0]['sw'], udic[0]['obs'], udic[0]['car'])

    # remove the digital filter
    data = ng.bruker.remove_digital_filter(dic, data)

    # process the spectrum
    data = ng.proc_base.zf_size(data, 32768)      # zero fill to 32768 points (speeds up fft)
    data = ng.proc_base.fft(data)                 # fourier transform
    data = ng.proc_autophase.autops(data, 'acme') # phase correction (automatic)
    data = ng.proc_base.rev(data)
    data = data / max(data)                       # normalize intensity to 1

    ppm = uc.ppm_scale()

    #axtop.set_xlim(hlimit[0], hlimit[1])
    axright.set_ylim(climit[0], climit[1])
    axright.set_xlim(cnmrlimit[0], cnmrlimit[1])
    #axtop.set_ylim(hnmrlimit[0], hnmrlimit[1])
    #axtop.plot(ppm, np.real(data), linewidth=.5, color='black')
    axright.plot(np.real(data), ppm, linewidth=.5, color='black')





hnmr()
cnmr()

# set the limits for the cosy
axmain.set_xlim(hlimit[1], hlimit[0])
axmain.set_ylim(climit[1], climit[0])

# make HNMRs nice
axtop.spines['top'].set_visible(False)
axtop.spines['left'].set_visible(False)
axtop.spines['right'].set_visible(False)
axtop.spines['bottom'].set_visible(False)
axtop.get_yaxis().set_visible(False)
axtop.get_xaxis().set_visible(False)

axright.spines['top'].set_visible(False)
axright.spines['left'].set_visible(False)
axright.spines['right'].set_visible(False)
axright.spines['bottom'].set_visible(False)
axright.get_yaxis().set_visible(False)
axright.get_xaxis().set_visible(False)

axmain.invert_xaxis()
axmain.invert_yaxis()

axmain.set_xlabel(r'$\delta_\text{H}$ / \si{ppm}', fontsize=16)
axmain.set_ylabel(r'$\delta_\text{C}$ / \si{ppm}', fontsize=16)

# make nice margins, create a grid and save
plt.tight_layout()
axmain.grid(color='gray',which='both',linestyle=':',linewidth=0.1)
plt.savefig("../plots/HSQC.pdf")

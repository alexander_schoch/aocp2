# Import libraries: numpy for advanced math stuff, matplotlib.pyplot for creating fancy plots
import numpy as np
import matplotlib.pyplot as plt
import pandas as pd
from scipy.signal import find_peaks


# Latex implementation: Process all text with latex
plt.rc('text', usetex=True)
plt.rc('text.latex', preamble=r'\usepackage{siunitx}')

dataraw = np.array(pd.read_csv('../data/UV/Sample417.Sample.Raw.asc', skiprows = 90, delimiter=',', header=None))
start = 500
istart = round((800 - start) * len(dataraw) / 550)
data = dataraw[0:istart]

peak = np.array(find_peaks(data[:,1])[0])[-1]

plt.text(data[peak,0], data[peak,1] - 0.002, str(round(data[peak,0])), rotation='vertical', va='top', ha='center')

plt.plot(data[:,0], data[:,1], color='black', linewidth=1)

# Axis labels
plt.xlabel(r'$\lambda$ / \si{\nano\meter}', fontsize=16)
plt.ylabel(r'Absorbance / -', fontsize=16)

# make margins nice and print the plot to a pdf file
plt.tight_layout()
plt.savefig('../plots/UV.pdf')

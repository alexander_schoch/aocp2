# Import libraries: numpy for advanced math stuff, matplotlib.pyplot for creating fancy plots
import numpy as np
import matplotlib.pyplot as plt
import pandas as pd
from scipy.signal import find_peaks

# Latex implementation: Process all text with latex
plt.rc('text', usetex=True)
plt.rc('text.latex', preamble=r'\usepackage{siunitx}')

data = pd.read_csv('../data/IR/IR.asp', header=None)
data_np = np.array(data[0])
wavenumber = np.linspace(4000,700,len(data))

peaks = np.array(find_peaks(np.array(100 - data_np))[0])
peaks_wn = np.array(wavenumber[peaks])
peaks_abs = np.array(data_np[peaks])

peaksofpeaks = np.array(find_peaks(np.array(100 - peaks_abs))[0])
peaks_wn = np.array(peaks_wn[peaksofpeaks])
peaks_abs = np.array(peaks_abs[peaksofpeaks])

fig, ax = plt.subplots()

peaks_wn = np.array([ 3505.9322033898306, 2920.5084745762715, 1716.1016949152545, 1578.1355932203392, 1503.5593220338983, 1402.8813559322034, 1248.1355932203392, 1018.813559322034, 759.6610169491528 ])
peaks_abs = np.array([ 74.3203936796281, 67.0784929754968, 46.535468612476, 42.7637228223484, 20.2250915196272, 23.1933276579844, 31.668047066470102, 39.278841016205504, 37.3726545107472 ]) 

for i in range(len(peaks_wn)):
    wn = peaks_wn[i]
    ab = peaks_abs[i]

    ax.text(wn, ab - 2, str(round(wn)), rotation='vertical', ha='center', va='top')

ax.plot(wavenumber, data, linewidth=.5, color='black')

ax.set_ylim(0,100)
ax.set_xlim(min(wavenumber), max(wavenumber))
ax.invert_xaxis()

# Axis labels
plt.xlabel(r'$\nu$ / \si{\per\centi\meter}', fontsize=16)
plt.ylabel(r'Transmittance / \si{\percent}', fontsize=16)

# make margins nice and print the plot to a pdf file
plt.tight_layout()
fig.savefig('../plots/IR.pdf')

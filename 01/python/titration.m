M_EDTA = 372.24;                          # g/mol
m_EDTA = 0.373;                           # g
V_EDTA = .1;                              # L
c_EDTA = m_EDTA / M_EDTA / V_EDTA;        # mol/L

# Sample
m_theor_sample = 0.6022;                  # g
M_sample = 222 + 58.69;                   # g/mol
n_theor_sample = m_theor_sample...
  / M_sample;                             # mol

# Addition before dilution
V_before = .01;                           # mL
n_before = c_EDTA * V_before / 11;        # mol

# mean value
titrations = [13.8, 13.5, 13.6] / 1000;   # mL
V_tit = mean(titrations);                 # L
n_tit = V_tit * c_EDTA;                   # mol
m_sample = (n_tit + n_before)...
  * M_sample * 11;                        # g, *11 because only 10 mL of 110 mL were taken
n = n_tit * 11 + n_before;
purity = n / n_theor_sample

# error propagation
stder_tit = std(titrations);              # L
stder_m_EDTA = 1e-4;                      # g
stder_V_EDTA = 1e-4;                      # L
  # c = m / (M * V)
stder_c_EDTA = ...
  sqrt((M_EDTA * V_EDTA)^-2 * stder_m_EDTA^2 ...
  +  (m_EDTA / (M_EDTA * V_EDTA^2))^2 * stder_V_EDTA^2); # mol/L
  # n = V * c + n_bef
stder_n = sqrt(c_EDTA^2 * stder_tit^2 + V_tit^2 * stder_c_EDTA^2); # mol 
  # purity = n / n_theor
stder_pur = sqrt(n_theor_sample^-2 * stder_n^2);
ci_pur = stder_pur * 4.302652729911275 / sqrt(3)

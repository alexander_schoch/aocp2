# AOCP II

## Template file for reports

You'll find the class file `aocp2.cls` in the `class/` directory. Also, a template file `template.tex` is found in the same folder

In order for it to work, the file has to be in the same folder as your latex source file.

### Usage:

Download the files using the Download button. Place `aocp2.cls` in your project folder and use `\documentclass{aocp2}`. Use `template.tex` to write your report (as all necessary commmands are in there already).

```
\documentclass{aocp2} 

\compound{Ethanol}
\author{Max Mustermann}{maxmu@student.ethz.ch}
\assistant{John Smith}{jsmith@student.ethz.ch}
\compoundimage{\includegraphics[width=\linewidth]{ethanol.png}}
\abstract{We made ethanol today!}

\begin{document}
  \maketitle

  \begin{multicols}{2}
    % Write Report here

  \end{multicols}
\end{document}
```

**Note for compoundimage**: As the background of the box is grey and the background of a ChemDraw-Figure white, I recommend you to export your molecule as a .png file. This way, the background is transparent.

### Custom Commands/Environments


#### Schemes

There is an envoronment called `scheme` for typesetting chemical reactions. It works exactly like a figure, but has the keyword "Scheme" in the caption.

All figures and Schemes have to be used with the `[H]` option, as floating environments do not work within the `multicols` environment. The same goes for `table`.

ChemDraw:
```
\begin{scheme}[H]
  \centering
  \includegraphics[width=\linewidth]{reaction.png}
  \caption{This is a nice reactioin}
\end{scheme}
```

chemfig:
```
\begin{scheme}[H]
  \centering
  \schemestart
    \chemfig{=[:30]-[:-30]=-[:30]}
    \arrow{->}
    \chemfig{=[:30]-[:-30]=-[:-90]}
  \schemestop
  \caption{This is a nice reactioin}
\end{scheme}
```

#### Code

To include code in your report, use the following command (ideally after `\end{multicols}`):

```
\lstinputlisting[language=Python, title=Caption]{path/to/file}
```

### Appendices

In order to include an appdenix (in one column), write the following after `\end{multicols}`:

```
\newpage
\appendix
\section{Appendix}
```

This will set your figure and section labelling to e.g. `A.1 Code` or `Figure A.2: H-NMR spectrum`.
